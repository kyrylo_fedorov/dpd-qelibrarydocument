/**
 * Author: kir
 * Created at: 27.11.14 18:22
 */
var fs = require('fs');
var utils = {};

module.exports = utils;

var voidCallback = function () {
};

/**
 * @param path
 * @returns {WriteStream}
 */
var createTarget = function createTarget(path) {
	return fs.createWriteStream(path).on('error', function (error) {
		ctx.done(error);
	});
};

/**
 *
 * @param sourcePath
 * @param targetPath
 * @param successCallback
 * @param dropSource Boolean Delete file after copy
 */
var fileAction = function fileAction(sourcePath, targetPath, successCallback, dropSource) {
	var doDropSource = dropSource === void 0 ? false : dropSource;
	var callback = typeof successCallback === "function" ? successCallback : function () {
		//console.log('default callback');
	};

	var source = fs.createReadStream(sourcePath)
		.on('error', function (error) {
			ctx.done(error);
		})
		.on('end', function () {
			if (doDropSource) {
				fs.unlink(sourcePath);
			}
		});

	var target = fs.createWriteStream(targetPath)
		.on('error', function (error) {
			ctx.done(error);
		})
		.on('finish', callback);


	source.pipe(target);
}




/**
 * Check directory for existing, create them if not
 *
 * @param directory Path to directory
 */
utils.createFolder = function createFolder(path) {
	try {
		fs.statSync(path).isDirectory();
	} catch (er) {
		fs.mkdir(path);
	}
};

/**
 * Copy file from sourcePath to targetPath, and call callback on success complete
 *
 * @param sourcePath
 * @param targetPath
 * @param callback
 */
utils.copyFile = function copyFile(sourcePath, targetPath, callback) {
	fileAction(sourcePath, targetPath, callback);
};

/**
 * Move file from sourcePath to targetPath, and call callback on success complete
 *
 * @param sourcePath
 * @param targetPath
 * @param callback
 */
utils.moveFile = function moveFile(sourcePath, targetPath, callback) {
	fileAction(sourcePath, targetPath, callback, true);
};

/**
 * Write file to targetPath from sourceStrem, oon complete call successCalback
 * @param sourceStream
 * @param targetPath
 * @param successCallback
 * @throws {Error} Send Error in response.
 */
utils.writeFile = function writeFile(sourceStream, targetPath, successCallback) {
	var callback = typeof successCallback === "function" ? successCallback : function () {
	};
	var target = createTarget(targetPath).on('finish', callback);
	sourceStream.pipe(target);
}

