/**
 * Author: kir
 * Date: 09.11.14 8:00
 */
"use strict";
var ObjectID = require('mongodb').ObjectID;
var util = require('util');
var constants = require('./LibraryEntryConstants');
var LibraryEntryDocument = require('./LibraryEntryDocument');

module.exports = {
	LibraryEntry: LibraryEntry,
	snapshotTypes: new SnapshotTypes()
};

function SnapshotTypes(){
	var ALLOW_SNAPSHOT_TYPE = {
		'contacts': {
			type: constants.TYPE.CONTACT_SNAPSHOT,
			defaultFilename: 'Contacts snapshot.pdf'
		},
		'sal': {
			type: constants.TYPE.SAL_SNAPSHOT,
			defaultFilename: 'SAL Report.pdf'
		},
		'asset': {
			type: constants.TYPE.ASSET_SNAPSHOT,
			defaultFilename: 'Statement of Assets.pdf'
		},
		'liability':{
			type: constants.TYPE.LIABILITY_SNAPSHOT,
			defaultFilename: 'Statement of Liabilities.pdf'
		}
	};
	var DEFAULT_FILE_NAME = {};
	DEFAULT_FILE_NAME[constants.TYPE.CONTACT_SNAPSHOT] = ALLOW_SNAPSHOT_TYPE.contacts.defaultFilename;
	DEFAULT_FILE_NAME[constants.TYPE.SAL_SNAPSHOT] = ALLOW_SNAPSHOT_TYPE.sal.defaultFilename;
	DEFAULT_FILE_NAME[constants.TYPE.ASSET_SNAPSHOT] = ALLOW_SNAPSHOT_TYPE.asset.defaultFilename;
	DEFAULT_FILE_NAME[constants.TYPE.LIABILITY_SNAPSHOT] = ALLOW_SNAPSHOT_TYPE.liability.defaultFilename;

	/**
	 * Resolve snapshot type by request parameter
	 * @param type String from request body
	 * @throws TypeError
	 * @returns {TYPE}
	 */
	this.resolve = function (type) {
		if (ALLOW_SNAPSHOT_TYPE[type] === void 0) {
			throw new TypeError('Unknown snapshot type specified');
		}
		return ALLOW_SNAPSHOT_TYPE[type].type;
	}

	this.getDefaultFileName = function(type){
		//console.log(DEFAULT_FILE_NAME[type]);

		return DEFAULT_FILE_NAME[type];
	}
};


/**
 * Library entry structure.
 * @type LibraryEntry
 * @param options - Initial values for entry
 * @returns {LibraryEntry}
 * @constructor
 */
function LibraryEntry(options) {
	var self = this;
	var TYPE = constants.TYPE;
	var TOUCH = constants.TOUCH;

	var defaultEntry = {
		//id: null,
		owner: null,
		type: TYPE.UNDEFINED,
		versions: [],
		access: [{
			date: Date.now(),
			mode: TOUCH.CREATE
		}],
		link: null,
		origin: null
		//get link() {
			//return this.id;
		//}
	};



	var privateEntry = util._extend(defaultEntry, options);
	//privateEntry.link = createLink();

	/**
	 * @TODO pretty link
	 */
	function createLink(){
		return privateEntry.id;
	}

	/**
	 * Add new version to entry
	 * @param document
	 * @returns {{$push: {versions: *}}} Object for modify mongo record
	 */
	this.addVersion = function (document) {
		privateEntry.versions.push(document);
		return {
				versions: document
		};
	};

	this.getVersion = function (versionId) {
		//return this.get;
		versionId = versionId || this.getLatestDocumentId();
		//if(versionId){
		//	throw new Error('getVersion by versionId not implement');
		//}
			var filtered = privateEntry.versions.filter(function(version){
				return version.id == versionId;
			});

//		console.log('found :', filtered);

		return !!filtered.length ? filtered[0] : false;
	};

	this.getLatestDocument = function(){
		return this.getVersion(this.getLatestDocumentId());
	};

	/**
	 * @todo fix fast and dirty getter
	 * @returns {*}
	 */
	this.getLatestDocumentId = function () {
		var latestModifiedDate = -Infinity;
		var latestVersionId;

		if(privateEntry.versions.length === 1){
			latestVersionId = privateEntry.versions[0].id;
		}else {

			privateEntry.access.forEach(function (touch) {
				/*
				 console.log(touch);
				 console.log('is update:', touch.mode & constants.TOUCH.UPDATE);
				 console.log('is create:', touch.mode & constants.TOUCH.CREATE);
				 console.log('latest date:', latestModifiedDate);
				 console.log('_this date: ', touch.date);
				 console.log('date is older', touch.date > latestModifiedDate);
				 */
				if ((touch.mode & constants.TOUCH.UPDATE || touch.mode & TOUCH.CREATE)
					&& (touch.date > latestModifiedDate)) {
					latestModifiedDate = touch.date;
					latestVersionId = touch.document_id;
				}
			});
		}
		//return privateEntry.versions[privateEntry.versions.length - 1];
		//return getVersion(privateEntry.versions.length - 1);
		//console.log('getLatestDocumentId:latest id: %s',latestVersionId);

		//return this.getVersion(latestVersionId);
		return latestVersionId;
	};
/*
	this.getLatestDocumentId = function(){
		return this.getLatestDocument().id;
	};
*/
	function getAccessListByDocumentId(documentId) {
		return privateEntry.access.filter(function (access) {
			return access.document_id === documentId
		})
	};

	/**
	 * Add new record to access[]
	 *
	 * @param options
	 * @return {{$push: {access: *}}} Object for modify mongo record
	 */
	this.touch = function (options) {
		var defaultTouch = {
			id: null,
			document_id: null,
			date: Date.now(),
			mode: constants.TOUCH.VIEW
		};
		var touch = util._extend(defaultTouch, options);

		if (!touch.document_id) {
			throw new Error('Unidentified document touch');
		}
		if(!touch.id){
			throw new Error('Undefined touch id');
		}

		privateEntry.access.push(touch);

		return {
				access: touch
		};
	};

	function getLatestTouches(documentId) {


		var documentAccess = getAccessListByDocumentId(documentId);
//console.log(documentAccess);

		return accessListToLastTouch(documentAccess);

	}

	/**
	 * Iterate access list, find latest dates
	 *
	 * @param accessList [{doc_id, accessDate, accessMode}]
	 * @return {{lastModified: timestamp, lastOpened: timestamp, lastAccessed: timestamp}}
	 */
	function accessListToLastTouch(accessList){
		var lastOpened = -Infinity;
		var lastModified = -Infinity;

		accessList.forEach(function (element) {
			/*
			 KISS !!!
			 var id = new ObjectID(element.id);
			 var date = id.getTimestamp();
			 */
			var date = element.date;
			var mode = element.mode;
			if (mode & constants.TOUCH.CREATE || mode & constants.TOUCH.UPDATE) {
				lastModified = Math.max(lastModified, date);
			}
			if (mode & constants.TOUCH.VIEW || mode & constants.TOUCH.DOWNLOAD) {
				lastOpened = Math.max(lastOpened, date);
			}
		});

		return {
			lastModified: lastModified,
			lastOpened: lastOpened,
			lastAccessed: Math.max(lastOpened, lastModified)
		};
	}

	function getLatestModified(document) {
		var lastModified = -Infinity;
		var targetDocumentAccess = getAccessListByDocumentId(document.id);

		targetDocumentAccess.forEach(function (element) {
			//var id = new ObjectID(element.id);
			//var date = id.getTimestamp();
			var date = element.date;
			var mode = element.mode;
			if (mode & TOUCH.CREATE || mode & TOUCH.UPDATE) {
				lastModified = Math.max(lastModified, date);
			}
		});

		return lastModified;
	}

	function decodeStatus(status){

	};

	this.setLink = function(){
		privateEntry.link = createLink();

		return {
			link: privateEntry.link
		};
	}

	Object.defineProperty(this, 'owner', {
		get: function(){
			return privateEntry.owner;
		}
	});

	Object.defineProperty(this, 'id', {
		get: function () {
			return privateEntry.id;
		},
		set: function(newId){
			privateEntry.id = newId;
		}
	});

	Object.defineProperty(this, 'link', {
		get: function(){
			return privateEntry.link;
		},
		set: function(link){
			privateEntry.link = link;
		}
	});

	Object.defineProperty(this, 'type', {
		get: function () {
			return privateEntry.type;
		},
		set: function (newType) {
			privateEntry.type = newType;
		}
	});




/*	Object.defineProperty(this, 'latestVersion', {

	});*/

/*
	Object.defineProperty(this, 'latest', {
		get: function () {
			var versions = privateEntry.versions;
			var lastDocument = new LibraryEntryDocument(versions.pop()).entry;

			var history = versions.map(function (document) {
				return {
					version_id: document.id,
					lastModified: getLatestModified(document)
				}
			});

			//var documentAccess = getLatestTouches(lastDocument.fileName);

			return util._extend({
				link: privateEntry.link,
				type: privateEntry.type,
				name: lastDocument.customName || lastDocument.name || 'NoName',
				status: lastDocument.status,
				category: lastDocument.category,
				dueDate: lastDocument.dueDate,
				comments: lastDocument.comments,
				history: history
			}, getLatestTouches(lastDocument.id));
		},
		set: function () {
			throw new TypeError('Property LibraryEntry.latest is readonly');
		}
	});
*/
	Object.defineProperty(this, 'storableEntry', {
		get: function(){
			return privateEntry;
		}
	});

	Object.defineProperty(this, 'entry', {

		get: function (){
			var latestVersionId = privateEntry.versions.length
				? this.getLatestDocumentId()
				: privateEntry.id;
//console.log('entry.latestVersionId: %s', latestVersionId);

			privateEntry.versions.forEach(function(version){
				version = util._extend(version, accessListToLastTouch(
					getAccessListByDocumentId(version.id)
				));
				return version;
			});

			var result = util._extend(privateEntry, {latest: latestVersionId});
			//console.log(privateEntry);
			return result;
		}
	});



};
