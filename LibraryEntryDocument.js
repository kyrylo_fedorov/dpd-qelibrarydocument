/**
 * Author: kir
 * Date: 09.11.14
 * Time: 7:58
 * Created with WebStorm.
 */
"use strict";
var util = require('util');
var constants = require('./LibraryEntryConstants');

module.exports = LibraryEntryDocument;

function LibraryEntryDocument(options){

	var STATUS = constants.DOCUMENT_STATUS;

	var defaultDocument = {
		id: null,
		fileId: null,
		fileName: '',
		customName: '',
		status: STATUS.DRAFT,
		category: null,
		dueDate: null,
		comments: ''
	};

	var document = util._extend(defaultDocument, options);

	this.parsePutRequest = function(content){

/*
		console.log('node_modules/dpd-QELibraryDocument/LibraryEntryDocument.js:31 \
		content: \
		', content);
*/
//console.log('content', content);

		document.customName = content.customName;
		document.status = content.status;
		//TODO save date after fix client side calendar
		//document.dueDate = new Date(content.dueDate).valueOf();
		document.dueDate = content.dueDate;
		document.comments = content.comments;

		//console.log('new document:', document);
		return this;
	}
	/**
	 * Compire current document with other, key by key, exept keys listed in exeption list.
	 *
	 * @param otherDocument
	 * @param [exeptedKeys] Array of keys thet exepted from compire
	 * @returns {Array} Diff report [{key, legend, thisValue, otherValue}]
	 */
	this.diff = function(otherDocument, exeptedKeys){
		exeptedKeys = exeptedKeys || [];
		var diff = [];
		var localKeys = Object.keys(document);
		var otherKeys = Object.keys(otherDocument);

		for(var i = 0, key; key = localKeys[i]; i++){
			if(otherDocument[key] === void 0){
				diff.push({
					key: key,
					legend: 'Value in other document not presented',
					thisValue: document[key],
					otherValue: undefined
				});
			}else if(document[key] !== otherDocument[key]){
				diff.push({
					key: key,
					legend: 'Values different',
					thisValue: document[key],
					otherValue: otherDocument[key]
				});
			}
		}
		for(var i = 0, key; key = otherKeys[i]; i++){
			if(document[key] === void 0){
				diff.push({
					key: key,
					legend: 'Value in this document not presented',
					thisValue: undefined,
					otherValue: otherDocument[key]
				});
			}
		}
		return !!diff.length ? diff : false;
	}

	Object.defineProperty(this, 'id', {
		get: function(){
			return document.id;
		},
		set: function(id){
			document.id = id
		}
	});

	Object.defineProperty(this, 'fileName', {
		get: function () {
			return document.fileName;
		},
		set: function(newFileName){
			document.fileName = newFileName;
		}
	});

	Object.defineProperty(this, 'fileId', {
		get: function () {
			return document.fileId;
		},
		set: function(newFileId){
			document.fileId = newFileId;
		}
	});


	Object.defineProperty(this, 'entry', {
		get: function(){
			return document;
		}
	})

	return this;
};

