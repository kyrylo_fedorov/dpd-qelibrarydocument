/**
 * Author: kir
 * Created at: 27.11.14 17:35
 */
module.exports = get;
var constants = require('../LibraryEntryConstants');
var LibraryEntry = require('../LibraryEntry').LibraryEntry;
var LibraryEntryDocument = require('../LibraryEntryDocument');
var util = require('util');
var mime = require('mime');
var fs = require('fs');

/**
 * GET handler
 *
 * GET /librarydocument - full list entries with latest document for each (similar to /library)
 * GET /librarydocument/:entry_id - Send file of latest document from specified entrie's in response
 * GET /librarydocument/:entry_id/:version_id - Send file the appropriate  version from specified library entry
 *
 * @param ctx
 * @param next
 */

function get(ctx, next) {

	if (this.currentUser === void 0) {
		//console.log('will stop');;
		ctx.end(new Error('Auth required', 403));
		return ctx.req.resume();
	}

	//console.log('get handler');

	var journal = this.store;
	var document;
	var currentUserId = this.currentUser.id;
	var shared = false;
	var userFileStorage = [this.config.fullDirectory, currentUserId].join('/');

	var entryId = false;
	var versionId = false;


	if (ctx.url && ctx.url !== '/') {
// request to specified entry
		var urlParts = ctx.url.split('/');
		try {
			entryId = urlParts[1]
		} catch (e) {
		}
		try {
			versionId = urlParts[2]
		} catch (e) {
		}
	}
//console.log('get:q:', ctx.query);

	if (entryId) {
		//console.log('get entry by id %s', entryId);

		journal.find({link: entryId, owner: currentUserId}, onFoundEntry);
	} else {
		journal.find({owner: currentUserId}, onFoundAllEntries);
	}

	function onFoundAllEntries(error, foundedEntries) {
		var entry;
		if (error) {
			ctx.done(error);
		}
		var result = [];
		for (var i = 0, entriesAmount = foundedEntries.length; i < entriesAmount; i++) {
			entry = new LibraryEntry(foundedEntries[i]);
			result.push(entry.entry);
		}
		ctx.done(error, result);
	}

	function onFoundEntry(error, foundedEntry) {
		//if (foundedEntry.owner !== currentUserId || shared) {
		//	ctx.done(new Error('Access forbiden', 403));
		//}

		if (foundedEntry.length === 0) {
			ctx.done(new Error('File not found', 404));
		}


		var entry = new LibraryEntry(foundedEntry[0]);


		if (versionId) {
			//console.log('try get version %s', versionId);
			try {
				document = entry.getVersion(versionId);
			} catch (e) {
				ctx.done(e);
			}
		} else {
			document = entry.getLatestDocument();
		}

		if (ctx.query.download !== void 0) {
			(function returnFile() {
				var systemFileName = [process.cwd(), userFileStorage, document.fileId].join('/');

				fs.stat(systemFileName, function (error, stats) {

					if (error) {
						ctx.done(new Error(error.code + '#' + error.errno));
						ctx.end();
					}
					if (!stats || !stats.isFile || !stats.isFile()) {
						ctx.done(new Error('Sorry, file not found.', 404));
						ctx.end();
					}
//console.log(error, stats);

					ctx.res.writeHead(200, {
						'Content-Type': mime.lookup(document.fileName),
						'Content-Length': stats['size'],
						'Content-Disposition': 'attachment; filename="' + document.fileName + '"'
					});

					fs.createReadStream(systemFileName)
						.on('open', function () {
							this.pipe(ctx.res);
						})
						.on('error', function (error) {
							ctx.done(error);
						})
						.on('end', function () {
							var touchMode = ctx.req.xhr
								? constants.TOUCH.VIEW
								: constants.TOUCH.DOWNLOAD;

							var touchNote = entry.touch({
								id: journal.createUniqueIdentifier(),
								document_id: document.id,
								mode: touchMode
							});

							journal.update({id: entry.id}, {$push: touchNote}, function (error, updatedEntry) {
								//ctx.done(error, updatedEntry);
								ctx.res.end();
							});
						});
				});
			})();
		} else {
			ctx.done(null, entry.entry);

		}


	}
}
