/**
 * Author: kir
 * Created at: 27.11.14 17:22
 */
module.exports = post;

var constants = require('../LibraryEntryConstants');
var LibraryEntry = require('../LibraryEntry').LibraryEntry;
var snapshotTypes = require('../LibraryEntry').snapshotTypes;


var LibraryEntryDocument = require('../LibraryEntryDocument');
var util = require('util');
var fsUtils = require('../lib/fsUtils');

/**
 * POST handler
 *
 * POST structure may content
 *  origin - string: [
 *    'html', - data contain raw html, usaly expected for create pdf
 *    'url', - data contain url template for create clone, can be local file or remot resource
 *    'file' - form contain uploaded file
 *  ]
 *  data - request contant
 *  [type] - specified sub type for entry
 *
 *  Request just to libary with:
 *    origin == 'html' && type:['contacts','sal'] - create new snapshot specified type, if this type snapshot are exists, update them with new version (call POST library/:exist_entry_id)
 *    origin == 'url' - create new entry(type=doc+template), copy template content (GET url) to new user's file as first version of entry, set name = file name from url
 *    origin == 'file' - create new entry for each file, pass upload file as first version of entry
 *
 *  Request can by perform to specific library entry (library/:exist_entry_id), in this case can be followed these options:
 *    origin == 'html' && type:['contacts','sal'] - new spanshot version
 *    origin == 'url' - can not be, response 400
 *    origin == 'file' - uploaded file (single file allowed! resp:400 otherwise) pass for update library entry
 *
 * @param ctx
 * @param next
 */

function post(ctx, next) {


	var userFileStoragePath = [this.config.fullDirectory, this.currentUser.id].join('/');
	var journal = this.store;
	var currentUser = this.currentUser;
	var ORIGIN_CONTACTS = 'contacts';
	var ORIGIN_SAL = 'sal';
	var ORIGIN_FILE = 'file';
	var ORIGIN_URL = 'url';
	var ORIGIN_TPL = 'tpl';
	var ORIGIN_ASSETS = 'asset';
	var ORIGIN_LIABILITIES = 'liability';

	//var origin = ctx.body.origin;
	var origin = (ctx.query.origin || ctx.body.origin || '«empty origin»').toLowerCase();
	var entryId = false;
	var versionId = false;

	if (ctx.url && ctx.url !== '/') {
// request to specified entry
		var urlParts = ctx.url.split('/');
		//console.log('urlParts: ',urlParts);

		try {
			entryId = urlParts[1]
		} catch (e) {
			//console.log('error on get url part 1', e);
		}
		;
		try {
			versionId = urlParts[2]
		} catch (e) {
			//console.log('error on get url part 2', e);
		}
		;
	}

	fsUtils.createFolder(userFileStoragePath);

	switch (origin) {
		case ORIGIN_TPL:
			//console.log('post');
			if(!ctx.req.internal){
				ctx.done(new Error('Access denied', 403));
			}
			createEntryRecord(ctx.body);
			break;
		case ORIGIN_FILE: // file(s) upload form
			processingUploadForm(entryId);
			break;
		case ORIGIN_CONTACTS:
		case ORIGIN_SAL:
		case ORIGIN_ASSETS:
		case ORIGIN_LIABILITIES:
		{
			// create pdf from html
			try {
				var snapshotType = snapshotTypes.resolve(origin);
			} catch (e) {
				ctx.done(e);
			}
			//try find same snapshot type in library
			journal.find({owner: currentUser.id, type: snapshotType},
				function (error, result) {
					var entry;
					if (error) {
						ctx.done(error);
					}

					//create new entry if not exists
					if (result.length === 0) {
						entry = new LibraryEntry({
							owner: currentUser.id,
							type: snapshotType,
						});
						journal.insert(entry.storableEntry, function (error, result) {
							if (error) {
								ctx.done(error);
							}
							journal.update({id: result.id}, entry.setLink(), function (error, updated) {
								if (error) {
									ctx.done(error);
								}
								createSnapshot(entry);
							});
						});

					} else {
						entry = new LibraryEntry(result[0]);
						createSnapshot(entry);
					}
				});
		}
			break;

		case ORIGIN_URL: //request for resource, then store them as file
			createCopyFromURL();
			break;

		default:
			ctx.done(new URIError('Unresolved origin: ' + origin));
	}

	//return ctx.req.resume();


	/**
	 * Create snapshot in .PDF from raw html passed by request.
	 * @param libraryEntry {LibraryEntry} Entry for store snapshot.
	 * @throws {Error} Send error to response
	 * @returns {LibraryEntry} Send updated library entry as response
	 */
	function createSnapshot(libraryEntry) {


		var wkhtmltopdf = require('wkhtmltopdf');
		var htmlContent = ctx.body.data;
		var wrappedHtmlContent =
			('<html><head>' + cssLinks() + '</head>' + htmlContent + '</html>')
				.replace(/(img src=").*logo\.png/, "$1" + fs.realpathSync("public/assets/images/logo.png"));

		var document = new LibraryEntryDocument({
			id: journal.createUniqueIdentifier(),
			fileId: journal.createUniqueIdentifier(),
			fileName: snapshotTypes.getDefaultFileName(libraryEntry.type)
		});

		var targteFileName = userFileStoragePath + '/' + document.fileId;


		fsUtils.writeFile(wkhtmltopdf(wrappedHtmlContent), targteFileName, function () {


			var modification = util._extend(
				libraryEntry.addVersion(document.entry),
				libraryEntry.touch({
					id: journal.createUniqueIdentifier(),
					document_id: document.id,
					mode: constants.TOUCH.UPDATE_FROM_CREATE
				})
			);

			journal.update({id: libraryEntry.id}, {$push: modification}, function (error, updated) {
//console.log('update for touch and version complete', arguments);

				ctx.done(error, libraryEntry.entry);
				//ctx.req.resume();
			});
		});
	}

	/**
	 * PDF creation helper
	 * @returns {string} Predefined CSS links
	 */
	function cssLinks() {
		var links = [
			'<link href="' + fs.realpathSync("public/assets/reset.css") + '" rel="stylesheet" media="all">',
			'<link href="' + fs.realpathSync("public/assets/typography.css") + '" rel="stylesheet" media="all">',
			'<link href="' + fs.realpathSync("public/assets/forms.css") + '" rel="stylesheet" media="all">',
			'<link href="' + fs.realpathSync("public/assets/layout.css") + '" rel="stylesheet" media="all">',
			'<style>.print-controls{display: none !important;}</style>'
		];

		return links.join('\n');
	}

	/**
	 * Request to supllied URL and store resived respoce to file, create library entry and add resived file to them
	 * @throws {Error} Send error in response if request to URL failed.
	 * @throws {Error} Send error in response if entry storing failed.
	 * @throws {Error} Send inherit from {fsUtils.writeFile} error in response if filw writing failed.
	 * @return {LibraryEntry} Create library entry
	 */
	function createCopyFromURL() {
		var url = ctx.body.data;
		var ownerId = currentUser.id;
		var originFileName = url.split('/').pop();

		var document = new LibraryEntryDocument({
			id: journal.createUniqueIdentifier(),
			fileId: journal.createUniqueIdentifier(),
			fileName: originFileName
		});


		var libraryEntry = new LibraryEntry({
			owner: ownerId,
			type: constants.TYPE.TEMPLATE_DOCUMENT,
			versions: [document.entry],
			access: [{
				document_id: document.id,
				date: Date.now(),
				mode: constants.TOUCH.CREATE_FROM_TEMPLATE
			}]
		});


		var targetFileName = userFileStoragePath + '/' + document.fileId;

		var onCopyComplete = function (error, result) {
			journal.insert(libraryEntry.entry, function (error, createdEntry) {
				var setLink = {$set: libraryEntry.setLink()};
				journal.update({id: createdEntry.id}, setLink, function (error, updateResult) {
					ctx.done(error, libraryEntry.entry);
				})
			});
		};

		if (/^http/.test(url)) {
			//exteranl request
			var request = require('request');

			fsUtils.writeFile(request.get(url).on('error', function (error) {
				ctx.done(error);
			}), targetFileName, onCopyComplete);
		} else {
			/*
			 //	/var/quickestate/dev.ca/toolkit/public/dnl/forms
			 //TODO запустить работу через dpd
			 var internalClient = require('deployd/lib/internal-client');
			 var dpd = internalClient.build(process.server, ctx.session);
			 dpd.req.session = ctx.session;
			 dpd.req.url = ctx.body.data;
			 dpd.req.url = fakeURL;
			 dpd.templateresource.get(function(data, err){
			 var targetFileName = userFileStoragePath + '/' + document.fileName;
			 fsUtils.writeFile(data, targetFileName, function (error, result) {
			 journal.insert(libraryEntry.entry, function (error, createdEntry) {
			 ctx.done(error, createdEntry);
			 });
			 })
			 });
			 */

			/*
			 var source = fs.createReadStream(__dirname + '/../../public' + url.replace(/.*\/templateresource/, ''))
			 .on('error', function (error) {
			 ctx.done(error);
			 })
			 .on('end', function () {
			 //	fs.unlink(sourcePath);
			 });

			 var target = fs.createWriteStream(targetFileName)
			 .on('error', function (error) {
			 ctx.done(error);
			 })
			 .on('finish', onCopyComplete);
			 source.pipe(target);
			 */
		}

	}

	function createEntryRecord(options) {

		var defaultOptions = {
			tplName: null,
			fileName: null,
			user: null
		};
		var parameters = util._extend(defaultOptions, options);

		if(!parameters.tplName || !parameters.fileName || !parameters.user
			|| !parameters.user.id){
			console.log('bad req', parameters);

			ctx.done(new Error('Bad Request. Invalid request parameters.', 400));
		}

		//	find doc by templateID
		journal.find({
			owner: currentUser.id,
			type: constants.TYPE.TEMPLATE_DOCUMENT,
			origin: parameters.tplName
		}, function (err, result) {
			if (err) {
				ctx.done(err);
			}

			if (result.length === 0) {
				console.log('create new doc');
				//	 create new document
				entry = new LibraryEntry({
					owner: currentUser.id,
					type: constants.TYPE.TEMPLATE_DOCUMENT,
					origin: parameters.tplName
				});

				journal.insert(entry.storableEntry, function (error, result) {
					if (error) {
						ctx.done(error);
					}
					journal.update({id: result.id}, entry.setLink(), function (error, updated) {
						if (error) {
							ctx.done(error);
						}
						createVersion(entry);
					});
				});
			} else {
				//console.log('use exists');
				entry = new LibraryEntry(result[0]);
				createVersion(entry);
			}
		});

		function createVersion(entry) {
			var version = new LibraryEntryDocument({
				id: journal.createUniqueIdentifier(),
				fileId: parameters.fileName,
				fileName: parameters.tplName,
			});

			var modification = util._extend(
				entry.touch({
					id: journal.createUniqueIdentifier(),
					document_id: version.id,
					mode: constants.TOUCH.CREATE_FROM_TEMPLATE
				}),
				entry.addVersion(version.entry)
			);

			journal.update({id: entry.id}, {$push: modification}, function (err, updated) {
				if (err) {
					ctx.done(err);
				}
				ctx.done(err, entry);
			});

		}
	}

	function processingUploadForm(entryId) {
		var formidable = require('formidable');
		var form = new formidable.IncomingForm();
		var uploadFiles = [];
		var handledFiles = [];
		var parsingComplete = false;
		var request = ctx.req;

		var onFileEvent = entryId ? updateEntry : createEntry;

		form
			.on('fileBegin', function (name, file) {
				uploadFiles.push({file: file, name: name});
			})
			.on('file', onFileEvent)
			.on('error', function (error) {
				ctx.done(error);
			})
			.on('end', function () {
				parsingComplete = true;
			})
			.parse(request);


		/**
		 * Parse uploaded form, for each file create new entry, move file to user folder, store entry to db. ON complete return response as [@LibraryEntry]
		 *
		 * @throws {Error} Send error to response
		 * @return [{LibraryEntry}] Send response with created entries
		 */
		function createEntry(name, file) {

			var libraryEntryDocument = new LibraryEntryDocument({
				id: journal.createUniqueIdentifier(),
				fileId: journal.createUniqueIdentifier(),
				fileName: file.name
			});

			var libraryEntry = new LibraryEntry({
				owner: currentUser.id,
				type: constants.TYPE.OWN_DOCUMENT,
				versions: [libraryEntryDocument.entry],
				access: [{
					//id: journal.createUniqueIdentifier(),
					date: Date.now(),
					document_id: libraryEntryDocument.id,
					mode: constants.TOUCH.CREATE_FROM_UPLOAD
				}]
			});

			var targetFileName = userFileStoragePath + '/' + libraryEntryDocument.id;

			var onMove = function () {
				// insert new entry to db
				journal.insert(libraryEntry.entry, function (error, journalRecord) {
					libraryEntry.id = journalRecord.id;
					var setLinkUpdate = {$set: libraryEntry.setLink()};
					//console.log('Update %s with',journalRecord.id, setLinkUpdate);

					journal.update({id: journalRecord.id}, setLinkUpdate, function (error, result) {
						handledFiles.push(libraryEntry.entry);
						if (parsingComplete && uploadFiles.length === handledFiles.length) {
							ctx.done(null, handledFiles);
						}
					});
				});
			};
			fsUtils.moveFile(file.path, targetFileName, onMove);
		}

		/**
		 * Upload file to exists library entry.
		 * Only <b>single</b> file will by stored!
		 *
		 * @throws {Error} Send error with response.
		 * @return {LibraryEntry} Responce with updated entry
		 */
		function updateEntry(name, file) {
			//console.log('update entry on file parsed');
			journal.find({owner: currentUser.id, id: entryId}, function (error, foundedEntry) {
				if (error) {
					ctx.done(error);
				}

				var entry = new LibraryEntry(foundedEntry);

				var libraryEntryDocument = new LibraryEntryDocument({
					id: journal.createUniqueIdentifier(),
					fileId: journal.createUniqueIdentifier(),
					fileName: file.name
				});

				var targetFileName = userFileStoragePath + '/' + libraryEntryDocument.fileId;

				var changes = util._extend(
					entry.addVersion(libraryEntryDocument.entry),
					entry.touch({
						id: journal.createUniqueIdentifier(),
						document_id: libraryEntryDocument.id,
						mode: constants.TOUCH.UPDATE_FROM_UPLOAD
					})
				);

				var onMove = function () {
					//console.log('file moved');
					journal.update({id: entry.id}, {$push: changes},
						function (err, journalRecord) {
							//console.log('entry updated');
							ctx.done(err, entry.entry);
						}
					);
				};
				fsUtils.moveFile(file.path, targetFileName, onMove);
			});
		}

	}

}
