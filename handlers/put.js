/**
 * Author: kir
 * Created at: 27.11.14 18:26
 */

module.exports = put;

var constants = require('../LibraryEntryConstants');
var LibraryEntry = require('../LibraryEntry').LibraryEntry;
var LibraryEntryDocument = require('../LibraryEntryDocument');
var util = require('util');

/**
 * Update existing library entry.
 *
 * Думаю здесь дело ограничется обновлением дополнителльных полей.
 * Get lates document from entry, modify them, add as new version to entry.
 *
 * @param ctx
 * @param next
 * @throws {Error} 400 In case entry_id not present
 * @throws {Error} 403 In case request try update not owned entry
 * @throws {Error} 404 In case requested entry not found
 * @return {LibraryEntry} Updated entry
 */
function put(ctx, next) {
	var journal = this.store;
	var urlParts = ctx.url.split('/');
	var document;
	var currentUserId = this.currentUser.id;
	var shared = false;
	var userFileStorage = [this.config.fullDirectory, currentUserId].join('/');

	if (urlParts[1] === void 0) {
		ctx.done(new Error('Entry identification is required', 400));
	}

	//console.log(urlParts);

	journal.find({id: urlParts[1]}, function (error, foundedEntry) {
		if (error) {
			return ctx.done(new Error(error, 404));
		}
		if (foundedEntry.owner !== currentUserId) {
			//console.log(foundedEntry.owner, currentUserId);

			return ctx.done(new Error('Access denied', 403));
		}

		var entry = new LibraryEntry(foundedEntry);
		var latestVersion = entry.getLatestDocument();
//console.log(latestVersion);

		var newDocument = new LibraryEntryDocument({
			id: journal.createUniqueIdentifier(),
			fileName: latestVersion.fileName,
			fileId: latestVersion.fileId
		});

		newDocument.parsePutRequest(ctx.body);

		//var editedDocument = entry.getVersion(ctx.body.versionId);

		//var mergedDocument = new LibraryEntryDocument(util._extend(editedDocument.entry, newDocument.entry));

		if (!newDocument.diff(latestVersion, ['id'])) {
			ctx.done(null, 'No changes');
		}

		//console.log('new doc:', newDocument.entry);

		var modification = util._extend(
			entry.touch({
				id: journal.createUniqueIdentifier(),
				document_id: newDocument.id,
				mode: constants.TOUCH.UPDATE
			}),
			entry.addVersion(newDocument.entry)
		);

		journal.update({id: entry.id}, {$push: modification}, function (error, updatedEntry) {
			ctx.done(error, entry.entry);
		});
	});
}
