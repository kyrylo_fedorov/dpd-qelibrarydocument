/**
 * Author: kir
 * Date: 09.11.14 7:41
 */

"use strict";

module.exports = QELibraryDocument;

var Resource = require('deployd/lib/resource');
var util = require('util');
//var debug = require('debug')('dpd-fileupload');
var fsUtils = require('./lib/fsUtils');
/*
var LibraryEntry = require('./LibraryEntry').LibraryEntry;
var LibraryEntryDocument = require('./LibraryEntryDocument');
var EntryTypes = require('./LibraryEntryConstants').TYPE;
var TouchTypes = require('./LibraryEntryConstants').TOUCH;
*/

/**
 * Module setup.
 * @param options
 * @constructor
 */
function QELibraryDocument(options) {

	Resource.apply(this, arguments);

	this.store = process.server.createStore(this.config.libraryStorage);

	this.config = {
		fullDirectory: global.appConfig.libraryPath
	};

	fsUtils.createFolder(this.config.fullDirectory);
}

util.inherits(QELibraryDocument, Resource);

QELibraryDocument.label = "QE library's documents upload/download";
QELibraryDocument.events = ["get", "post", 'put'];
QELibraryDocument.prototype.clientGeneration = true;
QELibraryDocument.basicDashboard = {
	settings: [
/*
TODO solve: it's correct
		{
		name: 'fileStorage',
		type: 'text',
		description: 'Directory for save the user files.'
	},
*/
		{
		name: 'libraryStorage',
		type: 'text',
		description: 'Collection for journaling user files.'
	}]
};

/**
 * Main handler for resource request
 *
 * @param ctx - Deployd context
 * @param next - Next action
 * @todo Solve: Change flow for calling event script after executing hundler
 */
QELibraryDocument.prototype.handle = function (ctx, next) {
	var self = this;
	var request = ctx.req;

	//console.log(ctx);

	if(ctx.req.internal){
		this.currentUser = ctx.body.user;
	}else {
		this.currentUser = ctx.session.user;
	}
	//console.log('currentUser:', this.currentUser);


	this.parts = ctx.url.split('/').filter(function (p) {
		return p;
	});

	this.result = {};

	this.domain = {
		url: ctx.url,
		parts: self.parts,
		query: ctx.query,
		body: ctx.body,
		'this': self.result,
		setResult: function (val) {
			self.result = val;
		}
	};

	(function (method) {
		var moduleHandler = self[method]
			? self[method]
			: function (ctx, next) {
			next();
		};

		if (self.events[method]) {
			self.events[method].run(ctx, self.domain, function (error) {
				if (error) {
					ctx.done(error);
				}
				moduleHandler.call(self, ctx, next);
			});
		} else {
			moduleHandler.call(self, ctx, next);
		}
	})(request.method.toLowerCase());
}

QELibraryDocument.prototype.post = require('./handlers/post')
QELibraryDocument.prototype.get = require('./handlers/get');
QELibraryDocument.prototype.put = require('./handlers/put');